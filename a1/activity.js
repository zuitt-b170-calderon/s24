// find users with letter 's' in their first name or 'd' in their last name

db.users.find(
	{
		$or: [
			{ firstName: {$regex: "s", $options: "i" }
			},
			{ lastName: {$regex: "d", $options: "i" }
			}
		]
	}
);
.pretty();

// find users who are from the HR sdepartment and their age is greater than or equal to 70

db.users.find(
	{
		$and:  [
		{
			department: "HR Department"
		},
		{
			age: { $gt: 70}
		}
	]

	}
)
.pretty();


// find users with the letter 'e' in their first name and has an age of less than or equal to 30

db.users.find(
	{
		$and: [
			{ firstName: {$regex: "e", $options: "i" }
			},
			{ age: { $lte: 30}
			}
		]
	}
)
.pretty();
